package co.com.reportes.controllers;

import co.com.reportes.JasperReportApplication;
import co.com.reportes.services.MainService;
import co.com.reportes.util.Utilitaria;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.NamingException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;

@RestController
@CrossOrigin(origins = {"*"}, allowedHeaders = {"*"})
public class MainController {

    private Logger LOG= LoggerFactory.getLogger(JasperReportApplication.class);
    @Autowired
    private MainService mainService;

    @Autowired
    ServletContext context;

    @Value("${reporte.fuid1}")
    String fuid1 ="";

    @Value("${reporte.fuid2}")
    String fuid2 ="";

    @Value("${jdni}")
    boolean jdni;

    @PostMapping("/reporteFuid")
    public void manual(HttpServletRequest request, HttpServletResponse response){

        String tipoReporte ="";
        JasperPrint jasperPrint;
        HashMap<String,Object> mapKey;
        try {
            mapKey=mainService.getValueJSONResponse(request);

            if(mapKey.get("ruta").equals("FUID1")){
                tipoReporte=fuid1;

            }else if(mapKey.get("ruta").equals("FUID2")){

                tipoReporte =fuid2;
            }

            tipoReporte = context.getRealPath(tipoReporte);
            System.out.println(tipoReporte);
            jasperPrint=mainService.conexionJasperReport(mapKey,tipoReporte, jdni);
            mainService.exportFile(jasperPrint,mapKey.get("formato").toString(),response,mapKey.get("fileName").toString());
        } catch (IOException | SQLException | JRException | NamingException e) {
            Utilitaria.responseError(response);
            e.printStackTrace();
        }
    }

    @GetMapping("/prueba")
    public String prueba(HttpServletRequest request, HttpServletResponse response){

        return "Pruebas Funciona";
    }



}
