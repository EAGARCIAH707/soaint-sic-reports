package co.com.reportes.services;

import co.com.reportes.JasperReportApplication;
import co.com.reportes.persistencia.DatasourceConfig;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
//import co.com.reportes.persistencia.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.stream.Collectors;

@Service
public class MainService {
    private Logger LOG = LoggerFactory.getLogger(JasperReportApplication.class);
    @Autowired
    private DatasourceConfig datasourceConfig;
    @Autowired
    private DataSource dataSource;

    public String getKeyJSONResponse(HttpServletRequest request, String key) throws IOException {
        StringBuffer jb = new StringBuffer();
        String line = null;
        BufferedReader reader = request.getReader();
        while ((line = reader.readLine()) != null)
            jb.append(line);
        LOG.info("esto es JSON:" + jb.toString());
        //Object obj= JSONValue.parse(jb.toString());
        JSONObject jo = new JSONObject(jb.toString());
        //JSONObject jsonObject = (JSONObject) obj;
        //String value = (String) jsonObject.get(key);
        String value = (String) jo.get(key);
        LOG.info("key: " + key);
        LOG.info("value: " + value);
        return value;
    }

    public HashMap<String, Object> getValueJSONResponse(HttpServletRequest request) throws IOException {
        HashMap<String, Object> mapKey = new HashMap<>();
        StringBuffer jb = new StringBuffer();
        String line = null;
        BufferedReader reader = request.getReader();
        while ((line = reader.readLine()) != null)
            jb.append(line);
        LOG.info("esto es JSON:" + jb.toString());

        //Object obj= JSONValue.parse(jb.toString());
        //JSONObject jsonObject = (JSONObject) obj;
        JSONObject jo = new JSONObject(jb.toString());
        //Iterator<String> keys = jsonObject.keySet().iterator();
        Iterator<String> keys = jo.keySet().iterator();

        while (keys.hasNext()) {
            String key = keys.next();
            if (key.equalsIgnoreCase("fase_archivo") && (jo.get(key) != null)) {
                switch (jo.get(key).toString()) {
                    case "AG":
                        mapKey.put("faseArchivo", "ARCHIVO DE GESTION");
                        break;
                    case "AC":
                        mapKey.put("faseArchivo", "ARCHIVO CENTRAL");
                        break;
                    case "AH":
                        mapKey.put("faseArchivo", "ARCHIVO HISTORICO");
                        break;
                    default:
                        mapKey.put("faseArchivo", "");
                        break;
                }

            } else {
                mapKey.put(key, jo.get(key).toString());
            }

            if (key.equalsIgnoreCase("estado") && (jo.get(key) != null)) {
                switch (jo.get(key).toString()) {
                    case "C":
                        mapKey.put("estado", "CERRADA");
                        break;
                    case "A":
                        mapKey.put("estado","ACTIVA");
                        break;
                    default:
                        mapKey.put("estado", "");
                        break;
                }

            }else{
                mapKey.put(key, jo.get(key).toString());
            }

            LOG.info("clave:" + key);
            LOG.info("valor:" + jo.get(key));
        }

        mapKey.values().removeAll(Collections.singleton(null));
        mapKey.values().removeAll(Collections.singleton("null"));
        mapKey.values().removeAll(Collections.singleton(""));

        System.out.println("reporteFuid :::: " + mapKey);

        return mapKey;
    }


    public JasperPrint conexionJasperReport(HashMap<String, Object> parametros, String ruta, boolean jdni) throws JRException, NamingException, SQLException {
        JasperPrint jasperPrint = null;
        System.out.println("parametros :::" + parametros);
        if (jdni) {
            LOG.info("Ingreso por la conexion JNDI usando el archivo de properties");
            //dataSource.getConnection();//crea el datasource con el jndi embebido
            //jasperPrint = JasperFillManager.fillReport(ruta, parametros, dataSource.getConnection());
            //jasperPrint = JasperFillManager.fillReport(ruta, parametros, dataSource.getConnection());
            jasperPrint = JasperFillManager.fillReport(ruta, null, datasourceConfig.getDataSourceJndi().getConnection());
        } else {
            LOG.info("Ingreso por la conexion JDBC usando el archivo de properties");
            //jasperPrint = JasperFillManager.fillReport(ruta, null, Conexion.getConnection());
            jasperPrint = JasperFillManager.fillReport(ruta, parametros, datasourceConfig.getDataSource().getConnection());
            //jasperPrint = JasperFillManager.fillReport(parametros.get("ruta"), null, datasourceConfig.getDataSource().getConnection());
        }
        return jasperPrint;
    }

    public void exportFile(JasperPrint jasperPrint, String formato, HttpServletResponse response, String filename) throws JRException, IOException {
        ServletOutputStream outputStream = null;
        String ext = formato.toUpperCase();
        switch (ext) {

            case "XLSX":
                response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + ".xlsx\";");
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                outputStream = response.getOutputStream();
                JRXlsxExporter exporter = new JRXlsxExporter();
                exporter.setExporterInput(new SimpleExporterInput(jasperPrint)); // set compiled report as input
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));  // set output file via path with filename
                SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
                configuration.setOnePagePerSheet(true); // setup configuration
                exporter.setConfiguration(configuration); // set configuration
                exporter.exportReport();
                break;

            case "PDF":
                response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + ".pdf\";");
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Pragma", "no-cache");
                response.setDateHeader("Expires", 0);
                response.setContentType("application/pdf");
                outputStream = response.getOutputStream();
                JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
                break;

            default:
                throw new JRException("formato deconocido...");
        }
    }
}
