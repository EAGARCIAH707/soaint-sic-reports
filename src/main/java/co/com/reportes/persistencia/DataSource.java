package co.com.reportes.persistencia;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {

    private static HikariConfig config = new HikariConfig();
    private static HikariDataSource ds;

    //bloque estatico
    /* Es posible declarar bloques de código como estáticos,
    de tal manera que sean ejecutados cuando se cargue la clase.
     Este tipo de bloques se conocen como bloques de inicialización estáticos (static initializer block).
    */
    /*esta funcionalidad permite dotar de cierta lógica la inicialización de variables de clase estáticas
     sin necesidad de incluirlo en su constructor
     */
    static {
        config.setJdbcUrl("jdbc:oracle:thin:@10.20.101.168:12001:business");
        config.setDataSourceClassName("oracle.jdbc.driver.OracleDriver");
        config.setUsername("DIGITALIZACION");
        config.setPassword("S1c2019");
        config.setDataSourceJNDI("java:/jdbc-digitalizacion");
        config.addDataSourceProperty("cachePrepStmts" , "true");
        config.addDataSourceProperty("prepStmtCacheSize" , "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit" , "2048");
        ds = new HikariDataSource( config );
    }

    private DataSource() {}

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}
