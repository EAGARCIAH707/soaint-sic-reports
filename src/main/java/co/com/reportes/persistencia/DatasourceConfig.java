package co.com.reportes.persistencia;

import co.com.reportes.JasperReportApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jndi.JndiTemplate;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;
import org.springframework.stereotype.Component;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

@Component
@PropertySource({"classpath:application.properties"})
public class DatasourceConfig {
    private Logger LOG= LoggerFactory.getLogger(JasperReportApplication.class);
    @Autowired
    private Environment env;

    public DataSource getDataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        LOG.info("propiedad: "+env.getRequiredProperty("spring.datasource.driver-class-name"));
        dataSource.setDriverClassName(env.getRequiredProperty("spring.datasource.driver-class-name"));
        dataSource.setUrl(env.getRequiredProperty("spring.datasource.url"));
        dataSource.setUsername(env.getRequiredProperty("spring.datasource.username"));
        dataSource.setPassword(env.getRequiredProperty("spring.datasource.password"));

        return dataSource;
    }

    //https://www.baeldung.com/spring-persistence-jpa-jndi-datasource
    public DataSource getDataSourceJndi() throws NamingException {
        LOG.info("jndi-name: "+env.getProperty("datasource.jndi-name"));
        return (DataSource) new JndiTemplate().lookup(env.getProperty("datasource.jndi-name"));
    }
}
