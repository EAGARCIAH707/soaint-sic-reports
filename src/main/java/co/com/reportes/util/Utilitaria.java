package co.com.reportes.util;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Utilitaria {

    public static void responseError(HttpServletResponse response)
    {
        response.setContentType("text/html");
        PrintWriter out = null;
        try {
            out = response.getWriter();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Se genero un error grave</title>");
        out.println("</head>");
        out.println("<body bgcolor=\"white\">");
        out.println("<h2>Se genero un error revise el log del servidor o consulte a su administrador...</h2>");
        out.println("</body>");
        out.println("</html>");
    }
}
